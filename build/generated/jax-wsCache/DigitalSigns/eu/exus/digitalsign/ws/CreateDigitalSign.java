
package eu.exus.digitalsign.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createDigitalSign complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createDigitalSign">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="digitalSign" type="{http://ws.digitalsign.exus.eu/}digitalSign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createDigitalSign", propOrder = {
    "digitalSign"
})
public class CreateDigitalSign {

    protected DigitalSign digitalSign;

    /**
     * Gets the value of the digitalSign property.
     * 
     * @return
     *     possible object is
     *     {@link DigitalSign }
     *     
     */
    public DigitalSign getDigitalSign() {
        return digitalSign;
    }

    /**
     * Sets the value of the digitalSign property.
     * 
     * @param value
     *     allowed object is
     *     {@link DigitalSign }
     *     
     */
    public void setDigitalSign(DigitalSign value) {
        this.digitalSign = value;
    }

}
