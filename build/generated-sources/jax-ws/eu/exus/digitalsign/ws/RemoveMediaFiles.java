
package eu.exus.digitalsign.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeMediaFiles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removeMediaFiles">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mediaFiles" type="{http://ws.digitalsign.exus.eu/}mediaFiles" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeMediaFiles", propOrder = {
    "mediaFiles"
})
public class RemoveMediaFiles {

    protected MediaFiles_Type mediaFiles;

    /**
     * Gets the value of the mediaFiles property.
     * 
     * @return
     *     possible object is
     *     {@link MediaFiles_Type }
     *     
     */
    public MediaFiles_Type getMediaFiles() {
        return mediaFiles;
    }

    /**
     * Sets the value of the mediaFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaFiles_Type }
     *     
     */
    public void setMediaFiles(MediaFiles_Type value) {
        this.mediaFiles = value;
    }

}
