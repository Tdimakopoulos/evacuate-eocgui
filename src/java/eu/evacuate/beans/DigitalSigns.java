/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.beans;

import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.DigitalSignDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import eu.exus.digitalsign.ws.DigitalSign;
import eu.exus.digitalsign.ws.DigitalSignMediaManager_Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class DigitalSigns {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/lists.exodussa.com/DigitalSignAPI/DigitalSignMediaManager.wsdl")
    private DigitalSignMediaManager_Service service;
    private java.util.List<eu.exus.digitalsign.ws.DigitalSign> precords = new ArrayList<eu.exus.digitalsign.ws.DigitalSign>();
    private eu.exus.digitalsign.ws.DigitalSign selected;
    private String ip;
    private String port;
    private String status;
    private Long fileplay;
    private String location;
    private String nickname;
    private List<String> nicknameslist;
    String dd1;
    private String nnfilename;
    private String nnfilename2;

    public void changeDs() {
        System.out.println("Selected Digital Sign is: " + nnfilename);
        nnfilename2 = nnfilename;
    }

    /**
     * Creates a new instance of DigitalSigns
     */
    public DigitalSigns() {
        nicknameslist = new ArrayList();
        LoadRecords();
        nnfilename2 = nnfilename;
    }

    public String saveadigitalsign() {

        eu.exus.digitalsign.ws.DigitalSign digitalSign = new eu.exus.digitalsign.ws.DigitalSign();

        digitalSign.setFileplay(new Long(0));

        digitalSign.setIp(this.ip);
        digitalSign.setLocation(this.location);
        digitalSign.setNickname(this.nickname);
        digitalSign.setPort(this.status);
        digitalSign.setStatus("ACTIVE");
        createDigitalSign(digitalSign);
        LoadRecords();
        return "ds?faces-redirect=true";
    }

    public String saveadigitalsignc() {


        LoadRecords();
        return "ds?faces-redirect=true";
    }

    public String saveadigitalsignEdit() {


        System.out.println("-->" + dd1);
        for (int i = 0; i < precords.size(); i++) {
            if (precords.get(i).getId().toString().equalsIgnoreCase(dd1)) {
                System.out.println("Located now edit");
                precords.get(i).setIp(ip);
                precords.get(i).setPort(port);
                precords.get(i).setNickname(nickname);
                precords.get(i).setLocation(location);

                System.out.println("nick " + nickname);
                System.out.println("Lock " + location);

                editDigitalSign(precords.get(i));


            }
        }
        LoadListRecords();
        return "ds?faces-redirect=true";
    }

    public String removedss() {
        String dd = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("userID").toString();
        System.out.println("selected : " + dd);
        for (int i = 0; i < precords.size(); i++) {
            if (precords.get(i).getId().toString().equalsIgnoreCase(dd)) {
                System.out.println("Located now remove");
                removeDigitalSign(precords.get(i));
            }
        }
        LoadRecords();
        LoadListRecords();
        return "ds?faces-redirect=true";
    }

    public String editdss() {
        String dd = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("userID").toString();
        System.out.println("selected : " + dd);
        dd1 = dd;
        for (int i = 0; i < precords.size(); i++) {
            if (precords.get(i).getId().toString().equalsIgnoreCase(dd)) {
                System.out.println("Located now remove");
                //       removeDigitalSign(precords.get(i));
                ip = precords.get(i).getIp();
                port = precords.get(i).getPort();
                nickname = precords.get(i).getNickname();
                location = precords.get(i).getLocation();


            }
        }
        LoadListRecords();
        return "EditDigitalSign?faces-redirect=true";
    }

    public String gotoadd() {
        return "AddDigitalSign?faces-redirect=true";

    }

    private void LoadRecords() {
        precords.clear();
        precords = findAllDigitalSigns();
        LoadListRecords();
    }

    private void LoadListRecords() {
        nicknameslist.clear();
        nicknameslist.add("Select Digital Sign");
        for (int i = 0; i < precords.size(); i++) {
            nicknameslist.add(precords.get(i).getNickname());
        }
    }

    private static java.util.List<eu.exus.digitalsign.ws.DigitalSign> findAllDigitalSigns() {
        eu.exus.digitalsign.ws.DigitalSigns_Service service = new eu.exus.digitalsign.ws.DigitalSigns_Service();
        eu.exus.digitalsign.ws.DigitalSigns port = service.getDigitalSignsPort();
        return port.findAllDigitalSigns();
    }

    private static void editDigitalSign(eu.exus.digitalsign.ws.DigitalSign digitalSign) {

        eu.exus.digitalsign.ws.DigitalSigns_Service service = new eu.exus.digitalsign.ws.DigitalSigns_Service();
        eu.exus.digitalsign.ws.DigitalSigns port = service.getDigitalSignsPort();
        port.editDigitalSign(digitalSign);
    }

    private static void createDigitalSign(eu.exus.digitalsign.ws.DigitalSign digitalSign) {
        eu.exus.digitalsign.ws.DigitalSigns_Service service = new eu.exus.digitalsign.ws.DigitalSigns_Service();
        eu.exus.digitalsign.ws.DigitalSigns port = service.getDigitalSignsPort();
        port.createDigitalSign(digitalSign);
    }

    private static void removeDigitalSign(eu.exus.digitalsign.ws.DigitalSign digitalSign) {
        eu.exus.digitalsign.ws.DigitalSigns_Service service = new eu.exus.digitalsign.ws.DigitalSigns_Service();
        eu.exus.digitalsign.ws.DigitalSigns port = service.getDigitalSignsPort();
        port.removeDigitalSign(digitalSign);
    }

    /**
     * @return the precords
     */
    public java.util.List<eu.exus.digitalsign.ws.DigitalSign> getPrecords() {
        return precords;
    }

    /**
     * @param precords the precords to set
     */
    public void setPrecords(java.util.List<eu.exus.digitalsign.ws.DigitalSign> precords) {
        this.precords = precords;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the fileplay
     */
    public Long getFileplay() {
        return fileplay;
    }

    /**
     * @param fileplay the fileplay to set
     */
    public void setFileplay(Long fileplay) {
        this.fileplay = fileplay;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return the selected
     */
    public eu.exus.digitalsign.ws.DigitalSign getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(eu.exus.digitalsign.ws.DigitalSign selected) {
        this.selected = selected;
    }

    /**
     * @return the nicknameslist
     */
    public List<String> getNicknameslist() {
        return nicknameslist;
    }

    /**
     * @param nicknameslist the nicknameslist to set
     */
    public void setNicknameslist(List<String> nicknameslist) {
        this.nicknameslist = nicknameslist;
    }

    /**
     * @return the nnfilename
     */
    public String getNnfilename() {
        return nnfilename;
    }

    /**
     * @param nnfilename the nnfilename to set
     */
    public void setNnfilename(String nnfilename) {
        this.nnfilename = nnfilename;
    }

    public String play() {

        String get = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pic_id");

        System.out.println("-->" + get);
        System.out.println("-->" + nnfilename2);
        PlayDS(nnfilename2, get);
        return "mmcc?faces-redirect=true";
    }

    public String stop() {

        String get = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pic_id");

        System.out.println("-->" + get);
        System.out.println("-->" + nnfilename2);
        stopMediaDS(nnfilename2);

        return "mmcc?faces-redirect=true";
    }

    public void stopMediaDS(String MediaSign) {
        CreateOperations pConnector = new CreateOperations();
        ControlDTO pControl = new ControlDTO();
        pControl.setSystems("OG,SOFIA");
        Long id1 = null;
        Long id2 = null;
        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DigitalSignDTO pDTO = new DigitalSignDTO();
        java.util.List<eu.exus.digitalsign.ws.DigitalSign> pDS = findAllDigitalSigns();
        for (int i = 0; i < pDS.size(); i++) {

            if (pDS.get(i).getNickname().equalsIgnoreCase(MediaSign)) {
                id1 = pDS.get(i).getId();
                stopMedia(id1);
                System.out.println("Media Sign ID To Stop : " + pDS.get(i).getId());
                pDTO.setProcedure(pDS.get(i).getId().toString());


            }
        }

        pDTO.setPlay("STOP");
        CreateOperations pCO = new CreateOperations();
        try {
            ResponseDTO pp = pCO.evacDigitalSign(pDTO);
            System.err.println("-->" + pp.getTaskId());
        } catch (IOException ex) {
            Logger.getLogger(DigitalSigns.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static java.util.List<eu.exus.digitalsign.ws.MediaFiles_Type> findAllMediaFiles() {
        eu.exus.digitalsign.ws.MediaFiles_Service service = new eu.exus.digitalsign.ws.MediaFiles_Service();
        eu.exus.digitalsign.ws.MediaFiles port = service.getMediaFilesPort();
        return port.findAllMediaFiles();
    }

    public void PlayDS(String MediaSign, String MediaFile) {
        CreateOperations pConnector = new CreateOperations();
        ControlDTO pControl = new ControlDTO();
        pControl.setSystems("OG,SOFIA");
        Long id1 = null;
        Long id2 = null;
        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DigitalSignDTO pDTO = new DigitalSignDTO();
        java.util.List<eu.exus.digitalsign.ws.DigitalSign> pDS = findAllDigitalSigns();
        for (int i = 0; i < pDS.size(); i++) {

            if (pDS.get(i).getNickname().equalsIgnoreCase(MediaSign)) {
                System.out.println("Media Sign ID : " + pDS.get(i).getId());
                pDTO.setProcedure(pDS.get(i).getId().toString());
                id1 = pDS.get(i).getId();
            }
        }
        java.util.List<eu.exus.digitalsign.ws.MediaFiles_Type> pFiles = findAllMediaFiles();
        for (int i = 0; i < pFiles.size(); i++) {
            if (pFiles.get(i).getFilename().equalsIgnoreCase(MediaFile)) {
                System.out.println("File ID : " + pFiles.get(i).getId());
                pDTO.setFilename("exus-mf-"+pFiles.get(i).getId().toString());
                id2 = pFiles.get(i).getId();
            }
        }

        pDTO.setPlay("PLAY");
        CreateOperations pCO = new CreateOperations();
        try {
            ResponseDTO pp = pCO.evacDigitalSign(pDTO);
            System.err.println("-->" + pp.getTaskId());
        } catch (IOException ex) {
            Logger.getLogger(DigitalSigns.class.getName()).log(Level.SEVERE, null, ex);
        }
        playMedia(id1, id2);
    }

    private String playMedia(java.lang.Long signID, java.lang.Long fileID) {
        eu.exus.digitalsign.ws.DigitalSignMediaManager port = service.getDigitalSignMediaManagerPort();
        return port.playMedia(signID, fileID);
    }

    private String stopMedia(java.lang.Long signID) {
        eu.exus.digitalsign.ws.DigitalSignMediaManager port = service.getDigitalSignMediaManagerPort();
        return port.stopMedia(signID);
    }
}
