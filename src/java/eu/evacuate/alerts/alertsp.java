/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.alerts;

import eu.esponder.journal.*;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Thomas
 */
@ManagedBean
@SessionScoped
public class alertsp implements Serializable {

    private DualListModel<String> susernames;

    /**
     * Creates a new instance of journalview
     */
    public alertsp() {
        List<String> citiesSource = new ArrayList<String>();
        List<String> citiesTarget = new ArrayList<String>();


        citiesSource.add("Thomas Room 201");

        citiesSource.add("Laura Room 303");

        citiesSource.add("Keith Room 405");

        citiesSource.add("Margaret Room 101");
        


        susernames = new DualListModel<String>(citiesSource, citiesTarget);
    }

    /**
     * @return the susernames
     */
    public DualListModel<String> getSusernames() {
        return susernames;
    }

    /**
     * @param susernames the susernames to set
     */
    public void setSusernames(DualListModel<String> susernames) {
        this.susernames = susernames;
    }
}
