/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.og.ws.dto;

/**
 *
 * @author tdim
 */
public class ControlDTO {
    private String systems;

    /**
     * @return the systems
     */
    public String getSystems() {
        return systems;
    }

    /**
     * @param systems the systems to set
     */
    public void setSystems(String systems) {
        this.systems = systems;
    }
    
}
