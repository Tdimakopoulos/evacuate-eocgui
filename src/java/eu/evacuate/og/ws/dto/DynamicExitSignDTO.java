/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.og.ws.dto;

/**
 *
 * @author tdim
 */
public class DynamicExitSignDTO {

    private String procedure;
    private String actionAES;

    /*
     
     * STATUS_UNKNOWN

     * STATUS_OFF

     * STATUS_RIGHT

     * STATUS_LEFT

     * STATUS_UP

     * STATUS_DOWN

     * STATUS_CLOSED

     * STATUS_BLINK
     */
    
    /**
     * @return the procedure
     */
    public String getProcedure() {
        return procedure;
    }

    /**
     * @param procedure the procedure to set
     */
    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    /**
     * @return the actionAES
     */
    public String getActionAES() {
        return actionAES;
    }

    /**
     * @param actionAES the actionAES to set
     */
    public void setActionAES(String actionAES) {
        this.actionAES = actionAES;
    }
}
