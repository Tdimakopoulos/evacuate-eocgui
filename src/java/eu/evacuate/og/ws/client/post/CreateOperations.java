package eu.evacuate.og.ws.client.post;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import eu.evacuate.og.ws.client.urlmanager.UrlManager;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.DigitalSignDTO;
import eu.evacuate.og.ws.dto.DynamicExitSignDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import eu.evacuate.og.ws.dto.STXPhoneDTO;
import eu.evacuate.og.ws.dto.TetraDTO;

public class CreateOperations {

    ObjectMapper mapper = new ObjectMapper();
    UrlManager URL = new UrlManager();

    public boolean evacControlSystem(
            ControlDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlControl());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return true;
    }
    
    public ResponseDTO evacTetra(
            TetraDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlTetra());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }
    
    public ResponseDTO evacDigitalSign(
            DigitalSignDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);
        System.out.println("Json to OG : "+ControlJSON);
        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlDigitalSign());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

    
    public ResponseDTO evacDigitalExitSign(
            DynamicExitSignDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlDynamicExitSign());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }
    
    public ResponseDTO evacSTXPhone(
            STXPhoneDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlSTXPhone());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }
    
}
