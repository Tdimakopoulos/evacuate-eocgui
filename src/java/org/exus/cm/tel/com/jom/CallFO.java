/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.cm.tel.com.jom;

import java.io.Serializable;

/**
 *
 * @author tdim
 */
public class CallFO implements Serializable {
    
    private String fname;
    private String ftype;
    private String fstatus;
    private String id;

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the ftype
     */
    public String getFtype() {
        return ftype;
    }

    /**
     * @param ftype the ftype to set
     */
    public void setFtype(String ftype) {
        this.ftype = ftype;
    }

    /**
     * @return the fstatus
     */
    public String getFstatus() {
        return fstatus;
    }

    /**
     * @param fstatus the fstatus to set
     */
    public void setFstatus(String fstatus) {
        this.fstatus = fstatus;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    
}
