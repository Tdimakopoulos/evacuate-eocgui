/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.cm.tel.com.jom;

import java.io.Serializable;

/**
 *
 * @author tdim
 */
public class calls implements Serializable {
    
    private String callmembers;
    private String calltype;
    private String initiator;
    private String duration;
    private String start;
    private String filename;
    private String status;

    /**
     * @return the callmembers
     */
    public String getCallmembers() {
        return callmembers;
    }

    /**
     * @param callmembers the callmembers to set
     */
    public void setCallmembers(String callmembers) {
        this.callmembers = callmembers;
    }

    /**
     * @return the calltype
     */
    public String getCalltype() {
        return calltype;
    }

    /**
     * @param calltype the calltype to set
     */
    public void setCalltype(String calltype) {
        this.calltype = calltype;
    }

    /**
     * @return the initiator
     */
    public String getInitiator() {
        return initiator;
    }

    /**
     * @param initiator the initiator to set
     */
    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return the start
     */
    public String getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
}
