/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.cm.tel.com.manager;

import org.exus.cm.tel.com.active.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.exus.cm.tel.com.jom.CallFO;
import org.exus.cm.tel.com.jom.calls;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class CallsManagerv implements Serializable {

    private List<CallFO> calls= new ArrayList();
    
    private CallFO selectedEn;
    
    
    
    /**
     * Creates a new instance of ActiveCallsManager
     */
    public CallsManagerv() {
        System.out.println("Initialize objects !!!!");
        calls.clear();
        CallFO pd=new CallFO();
        pd.setFname("Room 202");
        pd.setFstatus("online");
        pd.setFtype("Standard Room");
        calls.add(pd);
        
        CallFO pd1=new CallFO();
        pd1.setFname("Room 203");
        pd1.setFstatus("online");
        pd1.setFtype("Standard Room");
        calls.add(pd1);
        
        CallFO pd2=new CallFO();
        pd2.setFname("Rooms second deck");
        pd2.setFstatus("online");
        pd2.setFtype("All Rooms in second deck");
        calls.add(pd2);
        
        CallFO pd3=new CallFO();
        pd3.setFname("Rooms third deck");
        pd3.setFstatus("online");
        pd3.setFtype("All Rooms in third deck");
        calls.add(pd3);
        
        CallFO pd4=new CallFO();
        pd4.setFname("Room 301");
        pd4.setFstatus("online");
        pd4.setFtype("Standard Room");
        calls.add(pd4);
    }

    /**
     * @return the calls
     */
    public List<CallFO> getCalls() {
        
        calls.clear();
        CallFO pd=new CallFO();
        pd.setFname("Room 202");
        pd.setFstatus("online");
        pd.setFtype("Standard Room");
        calls.add(pd);
        
        CallFO pd1=new CallFO();
        pd1.setFname("Room 203");
        pd1.setFstatus("online");
        pd1.setFtype("Standard Room");
        calls.add(pd1);
        
        CallFO pd2=new CallFO();
        pd2.setFname("Rooms second deck");
        pd2.setFstatus("online");
        pd2.setFtype("All Rooms in second deck");
        calls.add(pd2);
        
        CallFO pd3=new CallFO();
        pd3.setFname("Rooms third deck");
        pd3.setFstatus("online");
        pd3.setFtype("All Rooms in third deck");
        calls.add(pd3);
        
        CallFO pd4=new CallFO();
        pd4.setFname("Room 301");
        pd4.setFstatus("online");
        pd4.setFtype("Standard Room");
        calls.add(pd4);
        return calls;
    }

    /**
     * @param calls the calls to set
     */
    public void setCalls(List<CallFO> calls) {
        this.calls = calls;
    }

    /**
     * @return the selectedEn
     */
    public CallFO getSelectedEn() {
        return selectedEn;
    }

    /**
     * @param selectedEn the selectedEn to set
     */
    public void setSelectedEn(CallFO selectedEn) {
        this.selectedEn = selectedEn;
    }

  
    
}
